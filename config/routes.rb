Rails.application.routes.draw do

  resources :pages
  # resource :service_spare, only: [:show] do
  get '/service_spare/:id', to: 'service_spare#show', as: 'service_spare'
  # end

  get 'search', to: 'search#index'

  get 'search/show', to: 'searche#show'

  resources :news_items
  resources :contacts

  get '/service/:id', to: 'service#show', as: 'service'

  get 'home/index'

  resources :categories, only: [:show], shallow: true do
    resources :products, only: [:show]
  end

  mount Ckeditor::Engine => '/ckeditor'
  root to: 'home#index'

  devise_for :users, controllers: { registrations: "registrations" }

  mount Uploader::Engine => '/uploader'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routinrdmg.html
  # root to: 'home@index'
end
