class Watermark < ApplicationRecord
  has_one :simple_picture, :as => :assetable, :dependent => :destroy, class_name: SimplePicture

  fileuploads :simple_picture

  rails_admin do
    label 'Водяной знак'

    edit do
      field :simple_picture do
        label { 'Водяной знак' }
      end
    end
  end
end
