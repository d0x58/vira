class Service < ApplicationRecord
  has_one :menu, as: :itemable
  rails_admin do
    label "Запчасти и сервис"

    # list do
    #   field :main do
    #     label { 'Главная?' }
    #   end
    # end

    edit do
      field :content, :ck_editor
    end
  end
end
