class Home < ApplicationRecord
  has_many :teasers, :as => :assetable, :dependent => :destroy
  fileuploads :teasers

  rails_admin do
    edit do
      field :about, :ck_editor
      field :teasers do
        label { 'Фото для салйдера' }
      end
    end
  end
end
