class Youtube < ApplicationRecord
    default_scope { order(position: :asc) }
    rails_admin do
      label 'Youtube'

      edit do
        fields :link, :desc, :position
      end

      show do
        fields :link, :desc, :position
      end
    end
end
