class Spec < ApplicationRecord

  default_scope { order(position: :asc) }
  has_one :image, :as => :assetable, :class_name => SpecPicture
  fileuploads :image

  rails_admin do
    label 'СпецПредложения'
    list do
      field :title
      field :desc
      field :title
      field :position
      field :link
      field :price
    end
    edit do
      field :title
      field :desc
      field :title
      field :position
      field :image
      field :link
      field :price
    end
  end
end
