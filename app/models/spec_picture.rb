class SpecPicture < Asset
  # belongs_to :product
  mount_uploader :file, SpecUploader

  def serializable_hash(options=nil)
    {
        "id" => id.to_s,
        "filename" => File.basename(file.path),
        "url" => file.url,
        "thumb_url" => file.url(:thumb),
        "public_token" => public_token,
    }
  end

  rails_admin do
    label 'Спец фото'
    edit do
      field :file do
        label { 'Фото' }
      end
      # exclude_fields :data_file_name
    end

    show do
      field :file do
        label { 'Фото' }
      end
      # exclude_fields :data_file_name
    end
  end
end
