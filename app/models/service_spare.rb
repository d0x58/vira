class ServiceSpare < ApplicationRecord
  has_many :menu, as: :itemable
  validates :title, presence: true

  rails_admin do
    label "Запчасть"

    list do
      field :id
      field :title
    end

    edit do
      field :title
      field :content, :ck_editor
    end
  end
end
