class Partner < ApplicationRecord

  has_one :image, :as => :assetable, :class_name => SimplePicture
  default_scope -> { order(position: :asc) }
  fileuploads :image

  rails_admin do
    label 'Партнеры'
  end
end
