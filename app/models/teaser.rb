class Teaser < Asset
  mount_uploader :file, TeaserUploader



  rails_admin do
    label 'Фото'

    edit do
      field :file do
        label  { 'Фото для слайдера' }
      end
      field :slider_desc
      field :slider_link
      # exclude_fields :data_file_name
    end

    show do
      field :file
      # exclude_fields :data_file_name
    end
  end
end
