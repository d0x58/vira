class Product < ApplicationRecord
  before_save :remove_empty_files

  STATUSES = [:unaval, :aval]

  has_many :pictures, :as => :assetable, :dependent => :destroy
  belongs_to :category

  enum status: STATUSES

  fileuploads :pictures

  def main
    pictures.find_by(main: true).try(:file).try(:url)
  end

  def price
    ActionController::Base.helpers.number_to_currency(read_attribute(:price), unit: "", separator: ",", delimiter: "")
  end
  def status_enum
    Hash[ ['Нет в наличии', 'В наличии'].zip(STATUSES) ]
  end

  validates :title, :category, :short_desc, :charact, presence: true

  rails_admin do

    list do
      fields :title, :price, :status, :category, :pictures
    end

    edit do
      field :title
      field :category
      field :short_desc, :ck_editor
      field :price
      field :status
      field :charact
      field :description1, :ck_editor
      field :description2, :ck_editor
      field :pictures
    end
  end

  def remove_empty_files
    pic_orig = self.pictures
    pic = pic_orig.select {|pic| pic if pic.file.url.present?}
    self.pictures = pic

    pic_for_delete = pic_orig.select {|pic| pic if pic.file.url.blank? && pic.id.present? }
    pic_for_delete.map(&:destroy!)
  end
end
