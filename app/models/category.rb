class Category < ApplicationRecord
  has_many :products
  has_many :menus, as: :itemable
  validates :title, :description, presence: true

  rails_admin do
    list do
      field :title
      # field :slug
      # field :position
      field :description
    end

    edit do
      field :title
      # field :slug
      # field :position
      field :description, :ck_editor
      field :products do
        label { 'Товары' }
      end
    end
  end
end
