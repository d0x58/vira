class Contact < ApplicationRecord

  has_one :menu, as: :itemable

  rails_admin do
    label "Контакты"

    list do
      field :id
      field :content
    end

    edit do
      field :content, :ck_editor
    end
  end
end
