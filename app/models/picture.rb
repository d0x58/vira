class Picture < Asset
  # belongs_to :product
  mount_uploader :file, PictureUploader

  before_save :set_main

  default_scope { order(main: :desc)  }


  def set_main
    self.main = self.main
  end


  def self.thumb_of_main
    pic = find_by(main: true) || first
    return pic.file.thumb.url if pic.present?
  end

  def self.main_or_first
    pic = find_by(main: true) || first
    return pic.file.url if pic.present?
  end

  def serializable_hash(options=nil)
    {
        "id" => id.to_s,
        "filename" => File.basename(file.path),
        "url" => file.url,
        "thumb_url" => file.url(:thumb),
        "public_token" => public_token,
    }
  end

  rails_admin do
    list do
      fields :id, :file, :main
    end

    edit do
      fields :file, :main
      # exclude_fields :data_file_name
    end

    show do
      fields :file, :main
      # exclude_fields :data_file_name
    end
  end
end
