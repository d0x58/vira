class Page < ApplicationRecord
  has_many :menu, as: :itemable
  validates :title, presence: true

  rails_admin do
    label "Стат. страница"

    edit do
      field :title
      field :content, :ck_editor
    end
  end
end
