class SpecItem < ApplicationRecord
  has_one :image, :as => :assetable, :class_name => SimplePicture
  fileuploads :image

  rails_admin do
    label 'Промо-блоки (слева)'

    list do
      field :position
      field :title
      # field :slug
      field :desc
      field :link
      field :link_text
      field :callback
      field :image do
        label { 'Изображение' }
      end
    end

    edit do
      field :title
      field :desc
      field :position
      field :link
      field :link_text
      field :callback
      field :image do
        label { 'Изображение' }
      end
    end
  end
end
