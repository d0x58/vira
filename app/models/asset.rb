class Asset < ActiveRecord::Base
  include Uploader::Asset
  # belongs_to :product

  def serializable_hash(options=nil)
    {
        "id" => id.to_s,
        "filename" => File.basename(file.path),
        "url" => file.url,
        "thumb_url" => file.url(:thumb),
        "public_token" => public_token,
    }
  end

  rails_admin do
      # visible false
    # list do
    # end
  end
end
