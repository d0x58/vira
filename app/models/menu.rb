class Menu < ApplicationRecord
  belongs_to :itemable, polymorphic: true

  default_scope -> { order(position: :asc) }
  # def resource_enum
  #    [['Active', 1],['Pending',0],['Banned',2]]
  # end
  validates :title, :position, :itemable, presence: true

  rails_admin do
    label 'Меню'

    edit do
      field :title
      # field :link
      field :position
      field :itemable
    end
  end
end
