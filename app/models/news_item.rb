class NewsItem < ApplicationRecord
  validates :title, :short_desc, presence: true
  has_one :simple_picture, :as => :assetable, :dependent => :destroy, class_name: SimplePicture

  fileuploads :simple_picture

  rails_admin do
    label "Новости"

    list do
      field :id
      field :title
      field :short_desc
      field :simple_picture
    end

    edit do
      field :title
      field :short_desc, :ck_editor
      field :content, :ck_editor
      field :simple_picture
    end
  end
end
