module ApplicationHelper

  def bread(item)
    result = ''
    tpl = <<~TPL
     <div class="ui container">
        <div class="ui breadcrumb">
           <a class="section" href="/">Главная</a>
           <i class="my-red circle icon divider"></i>
           %{path}
        </div>
     </div>
    TPL

    if item.class.name == 'Product'
      category = item.category
      category_title = category.title.capitalize
      path = ''

      path << "<a class='section' href='#{category_path(category)}'> %{cat} </a>" % {cat: category_title}
      path << '<i class="my-red circle icon divider"></i>'

      path << "<a class='section active'> %{prod} </a>" % {prod: item.title.capitalize}

      result << tpl % {path: path}
    elsif item.class.name == 'Category'
      category = item
      category_title = category.title.capitalize
      path = ''

      path << "<a class='section active' href='#{category_path(category)}'> %{cat} </a>" % {cat: category_title}

      result << tpl % {path: path}
    elsif item.class.name == 'Service'
      path = ''

      path << "<a class='section active' href='#'> %{cat} </a>" % {cat: 'Сервис и запчасти'}
      result << tpl % {path: path}
    elsif item.class.name == 'ServiceSpare'
      path = ''
      path << "<a class='section' href='/service'> %{cat} </a>" % {cat: 'Сервис и запчасти'}
      path << '<i class="my-red circle icon divider"></i>'
      path << "<a class='section active' href='#'> %{cat} </a>" % {cat: item.title}
      result << tpl % {path: path}
    elsif item.class.name == 'Contact'
      path = ''

      path << "<a class='section active' href='#'> %{cat} </a>" % {cat: 'Контакты'}
      result << tpl % {path: path}
    elsif item.class.name == 'NewsItem'
      path = ''
      # path << "<a class='section' href='/news_items'> %{cat} </a>" % {cat: 'Новости'}
      # path << '<i class="my-red circle icon divider"></i>'
      path << "<a class='section active' href='#'> %{cat} </a>" % {cat: item.title}
      result << tpl % {path: path}
    end

    result
  end
  def menu_items
    Category.all.order(position: :asc)
  end

  def char_to_row(pr)
    result = ''

    return result if pr.charact.blank?

    ch = pr.charact.split("\n").map(&:strip)
    ch.each do |c|
      ch_td = c.split('|')
      result << "<tr><td>#{ch_td[0]}</td><td>#{ch_td[1]}</td></tr>"
    end

    result
  end
  def presence(item)
    item.status == 'aval' ? '<span class="presence--true">В наличии</span>' : '<span class="presence--false">Нет в наличии</span>'
  end
end
