class ServiceSpareController < ApplicationController
  def show
    @spare = ServiceSpare.find(params[:id]) || ServiceSpare.new
  end
end
