class NewsItemsController < ApplicationController
  def index
    @news = NewsItem.all.order(created_at: :asc)
  end

  def show
    @news = NewsItem.find(params[:id]) || NewsItem.new
  end
end
