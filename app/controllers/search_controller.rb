class SearchController < ApplicationController
  def index
    @q = params[:title]
    @products = Product.where("title like ?", "%#{params[:title]}%")
  end

  def show
  end
end
