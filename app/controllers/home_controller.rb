class HomeController < ApplicationController
  def index
    @yt = Youtube.all
    @news = NewsItem.all
    @specs = Spec.all
    @home = Home.first
    @products = Product.all
  end
end
