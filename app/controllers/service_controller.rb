class ServiceController < ApplicationController
  def show
    @service = Service.find(params[:id]) || Service.new
  end
end
