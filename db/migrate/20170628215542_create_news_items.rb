class CreateNewsItems < ActiveRecord::Migration[5.1]
  def change
    create_table :news_items do |t|
      t.string :title
      t.text :short_desc
      t.text :content
      t.string :image

      t.timestamps
    end
  end
end
