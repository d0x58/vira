class CreateSpecItems < ActiveRecord::Migration[5.1]
  def change
    create_table :spec_items do |t|
      t.string :title
      t.string :desc
      t.string :link
      t.string :link_text
      t.boolean :callback
      t.integer :position

      t.timestamps
    end
  end
end
