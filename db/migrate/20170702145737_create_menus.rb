class CreateMenus < ActiveRecord::Migration[5.1]
  def change
    create_table :menus do |t|
      t.string :title
      t.string :link
      t.integer :position
      t.integer :itemable_id
      t.string :itemable_type

      t.timestamps
    end
  end
end
