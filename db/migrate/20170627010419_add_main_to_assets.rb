class AddMainToAssets < ActiveRecord::Migration[5.1]
  def change
    add_column :assets, :main, :boolean
  end
end
