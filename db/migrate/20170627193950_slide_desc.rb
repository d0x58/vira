class SlideDesc < ActiveRecord::Migration[5.1]
  def change
    add_column :assets, :slider_desc, :string
    add_column :assets, :slider_link, :string
  end
end
