class CreateCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :categories do |t|
      t.string :title, null: false
      t.text :description, null: false
      t.text :slug, null: false

      t.timestamps
    end

    add_index :categories, :slug, unique: true
  end
end
