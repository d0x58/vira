class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :title, null: false
      t.decimal :price, :precision => 15, :scale => 10, default: 0
      t.integer :status, default: 0
      t.text :description1
      t.text :description2

      t.timestamps
    end

    add_index :products, :title
  end
end
