class ServiceSpare < ActiveRecord::Migration[5.1]
  def change
    create_table :service_spares do |t|
      t.string :title
      t.text :content

      t.timestamps
    end
  end
end
