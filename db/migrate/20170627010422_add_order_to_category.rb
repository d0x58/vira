class AddOrderToCategory < ActiveRecord::Migration[5.1]
  def change
    add_column :categories, :position, :integer
    add_index :categories, :position
  end
end
