class AddAbout < ActiveRecord::Migration[5.1]
  def change
    change_table :homes do |t|
      t.text :about
    end
  end
end
