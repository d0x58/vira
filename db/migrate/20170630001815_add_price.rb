class AddPrice < ActiveRecord::Migration[5.1]
  def change
    change_table :specs do |t|
    t.string :price
    end
  end
end
