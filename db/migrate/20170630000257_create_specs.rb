class CreateSpecs < ActiveRecord::Migration[5.1]
  def change
    create_table :specs do |t|
      t.string :title
      t.string :desc
      t.integer :position
      t.string :link
      t.timestamps
    end
  end
end
