class ChandeSl < ActiveRecord::Migration[5.1]
  def change
    change_column :categories, :slug, :string, :null => true
  end
end
