class CreateYoutubes < ActiveRecord::Migration[5.1]
  def change
    create_table :youtubes do |t|
      t.string :link
      t.string :desc
      t.integer :position

      t.timestamps
    end
  end
end
