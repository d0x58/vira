--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.5
-- Dumped by pg_dump version 9.5.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: localuser
--

CREATE TABLE ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE ar_internal_metadata OWNER TO localuser;

--
-- Name: assets; Type: TABLE; Schema: public; Owner: localuser
--

CREATE TABLE assets (
    id bigint NOT NULL,
    file character varying,
    data_file_name character varying,
    data_content_type character varying,
    data_file_size integer,
    assetable_id integer NOT NULL,
    assetable_type character varying(30) NOT NULL,
    type character varying(30),
    guid character varying(20),
    public_token character varying(20),
    user_id integer,
    sort_order integer DEFAULT 0,
    width integer,
    height integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    main boolean,
    slider_desc character varying,
    slider_link character varying
);


ALTER TABLE assets OWNER TO localuser;

--
-- Name: assets_id_seq; Type: SEQUENCE; Schema: public; Owner: localuser
--

CREATE SEQUENCE assets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE assets_id_seq OWNER TO localuser;

--
-- Name: assets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: localuser
--

ALTER SEQUENCE assets_id_seq OWNED BY assets.id;


--
-- Name: categories; Type: TABLE; Schema: public; Owner: localuser
--

CREATE TABLE categories (
    id bigint NOT NULL,
    title character varying NOT NULL,
    description text NOT NULL,
    slug text NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    "position" integer
);


ALTER TABLE categories OWNER TO localuser;

--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: localuser
--

CREATE SEQUENCE categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE categories_id_seq OWNER TO localuser;

--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: localuser
--

ALTER SEQUENCE categories_id_seq OWNED BY categories.id;


--
-- Name: ckeditor_assets; Type: TABLE; Schema: public; Owner: localuser
--

CREATE TABLE ckeditor_assets (
    id bigint NOT NULL,
    data_file_name character varying NOT NULL,
    data_content_type character varying,
    data_file_size integer,
    type character varying(30),
    width integer,
    height integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE ckeditor_assets OWNER TO localuser;

--
-- Name: ckeditor_assets_id_seq; Type: SEQUENCE; Schema: public; Owner: localuser
--

CREATE SEQUENCE ckeditor_assets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ckeditor_assets_id_seq OWNER TO localuser;

--
-- Name: ckeditor_assets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: localuser
--

ALTER SEQUENCE ckeditor_assets_id_seq OWNED BY ckeditor_assets.id;


--
-- Name: contacts; Type: TABLE; Schema: public; Owner: localuser
--

CREATE TABLE contacts (
    id bigint NOT NULL,
    content text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE contacts OWNER TO localuser;

--
-- Name: contacts_id_seq; Type: SEQUENCE; Schema: public; Owner: localuser
--

CREATE SEQUENCE contacts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE contacts_id_seq OWNER TO localuser;

--
-- Name: contacts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: localuser
--

ALTER SEQUENCE contacts_id_seq OWNED BY contacts.id;


--
-- Name: homes; Type: TABLE; Schema: public; Owner: localuser
--

CREATE TABLE homes (
    id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    about text
);


ALTER TABLE homes OWNER TO localuser;

--
-- Name: homes_id_seq; Type: SEQUENCE; Schema: public; Owner: localuser
--

CREATE SEQUENCE homes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE homes_id_seq OWNER TO localuser;

--
-- Name: homes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: localuser
--

ALTER SEQUENCE homes_id_seq OWNED BY homes.id;


--
-- Name: news_items; Type: TABLE; Schema: public; Owner: localuser
--

CREATE TABLE news_items (
    id bigint NOT NULL,
    title character varying,
    short_desc text,
    content text,
    image character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE news_items OWNER TO localuser;

--
-- Name: news_items_id_seq; Type: SEQUENCE; Schema: public; Owner: localuser
--

CREATE SEQUENCE news_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE news_items_id_seq OWNER TO localuser;

--
-- Name: news_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: localuser
--

ALTER SEQUENCE news_items_id_seq OWNED BY news_items.id;


--
-- Name: products; Type: TABLE; Schema: public; Owner: localuser
--

CREATE TABLE products (
    id bigint NOT NULL,
    title character varying NOT NULL,
    price numeric(15,10) DEFAULT 0,
    status integer DEFAULT 0,
    description1 text,
    description2 text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    category_id integer,
    short_desc text,
    charact text
);


ALTER TABLE products OWNER TO localuser;

--
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: localuser
--

CREATE SEQUENCE products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE products_id_seq OWNER TO localuser;

--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: localuser
--

ALTER SEQUENCE products_id_seq OWNED BY products.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: localuser
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE schema_migrations OWNER TO localuser;

--
-- Name: service_spares; Type: TABLE; Schema: public; Owner: localuser
--

CREATE TABLE service_spares (
    id bigint NOT NULL,
    title character varying,
    content text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE service_spares OWNER TO localuser;

--
-- Name: service_spares_id_seq; Type: SEQUENCE; Schema: public; Owner: localuser
--

CREATE SEQUENCE service_spares_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE service_spares_id_seq OWNER TO localuser;

--
-- Name: service_spares_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: localuser
--

ALTER SEQUENCE service_spares_id_seq OWNED BY service_spares.id;


--
-- Name: services; Type: TABLE; Schema: public; Owner: localuser
--

CREATE TABLE services (
    id bigint NOT NULL,
    content text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE services OWNER TO localuser;

--
-- Name: services_id_seq; Type: SEQUENCE; Schema: public; Owner: localuser
--

CREATE SEQUENCE services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE services_id_seq OWNER TO localuser;

--
-- Name: services_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: localuser
--

ALTER SEQUENCE services_id_seq OWNED BY services.id;


--
-- Name: spec_items; Type: TABLE; Schema: public; Owner: localuser
--

CREATE TABLE spec_items (
    id bigint NOT NULL,
    title character varying,
    "desc" character varying,
    link character varying,
    link_text character varying,
    callback boolean,
    "position" integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE spec_items OWNER TO localuser;

--
-- Name: spec_items_id_seq; Type: SEQUENCE; Schema: public; Owner: localuser
--

CREATE SEQUENCE spec_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spec_items_id_seq OWNER TO localuser;

--
-- Name: spec_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: localuser
--

ALTER SEQUENCE spec_items_id_seq OWNED BY spec_items.id;


--
-- Name: specs; Type: TABLE; Schema: public; Owner: localuser
--

CREATE TABLE specs (
    id bigint NOT NULL,
    title character varying,
    "desc" character varying,
    "position" integer,
    link character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    price character varying
);


ALTER TABLE specs OWNER TO localuser;

--
-- Name: specs_id_seq; Type: SEQUENCE; Schema: public; Owner: localuser
--

CREATE SEQUENCE specs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE specs_id_seq OWNER TO localuser;

--
-- Name: specs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: localuser
--

ALTER SEQUENCE specs_id_seq OWNED BY specs.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: localuser
--

CREATE TABLE users (
    id bigint NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip inet,
    last_sign_in_ip inet,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE users OWNER TO localuser;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: localuser
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO localuser;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: localuser
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: youtubes; Type: TABLE; Schema: public; Owner: localuser
--

CREATE TABLE youtubes (
    id bigint NOT NULL,
    link character varying,
    "desc" character varying,
    "position" integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE youtubes OWNER TO localuser;

--
-- Name: youtubes_id_seq; Type: SEQUENCE; Schema: public; Owner: localuser
--

CREATE SEQUENCE youtubes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE youtubes_id_seq OWNER TO localuser;

--
-- Name: youtubes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: localuser
--

ALTER SEQUENCE youtubes_id_seq OWNED BY youtubes.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY assets ALTER COLUMN id SET DEFAULT nextval('assets_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY categories ALTER COLUMN id SET DEFAULT nextval('categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY ckeditor_assets ALTER COLUMN id SET DEFAULT nextval('ckeditor_assets_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY contacts ALTER COLUMN id SET DEFAULT nextval('contacts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY homes ALTER COLUMN id SET DEFAULT nextval('homes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY news_items ALTER COLUMN id SET DEFAULT nextval('news_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY products ALTER COLUMN id SET DEFAULT nextval('products_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY service_spares ALTER COLUMN id SET DEFAULT nextval('service_spares_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY services ALTER COLUMN id SET DEFAULT nextval('services_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY spec_items ALTER COLUMN id SET DEFAULT nextval('spec_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY specs ALTER COLUMN id SET DEFAULT nextval('specs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY youtubes ALTER COLUMN id SET DEFAULT nextval('youtubes_id_seq'::regclass);


--
-- Data for Name: ar_internal_metadata; Type: TABLE DATA; Schema: public; Owner: localuser
--

COPY ar_internal_metadata (key, value, created_at, updated_at) FROM stdin;
environment	development	2017-06-26 11:58:09.874597	2017-06-26 11:58:09.874597
\.


--
-- Data for Name: assets; Type: TABLE DATA; Schema: public; Owner: localuser
--

COPY assets (id, file, data_file_name, data_content_type, data_file_size, assetable_id, assetable_type, type, guid, public_token, user_id, sort_order, width, height, created_at, updated_at, main, slider_desc, slider_link) FROM stdin;
56	sp2.png	\N	\N	\N	1	Spec	SpecPicture	\N	Z9t2rKRqyy2FH4DA7vun	\N	0	\N	\N	2017-06-30 05:17:30.811736	2017-06-30 05:37:28.637375	\N	\N	\N
58	sp3.png	\N	\N	\N	3	Spec	SpecPicture	\N	YfdnRbmkMFgzDxeVTrYH	\N	0	\N	\N	2017-06-30 05:36:13.696912	2017-06-30 05:37:44.513829	\N	\N	\N
57	sp1.png	\N	\N	\N	2	Spec	SpecPicture	\N	eNYrvq4v0yn5jQinyx7Z	\N	0	\N	\N	2017-06-30 05:35:04.12367	2017-06-30 05:37:52.975452	\N	\N	\N
41	10.jpg	\N	\N	\N	1	NewsItem	SimplePicture	\N	Y0tMdrIVahDF4LhGWrjt	\N	0	\N	\N	2017-06-29 03:32:30.495019	2017-06-29 03:59:57.240954	\N	\N	\N
42	item2.png	\N	\N	\N	1	SpecItem	SimplePicture	\N	BuHfTmQld274LEdUb3Im	\N	0	\N	\N	2017-06-29 04:39:03.10474	2017-06-29 04:39:03.10474	\N	\N	\N
43	spec.png	\N	\N	\N	2	SpecItem	SimplePicture	\N	C2QzDPwjN97XObxt36tH	\N	0	\N	\N	2017-06-29 04:48:36.465538	2017-06-29 04:48:36.465538	\N	\N	\N
44	news-item.png	\N	\N	\N	3	SpecItem	SimplePicture	\N	i1pCv3KgAyAyH0RotRpy	\N	0	\N	\N	2017-06-29 04:50:46.440252	2017-06-29 04:50:46.440252	\N	\N	\N
45	item1.png	\N	\N	\N	4	SpecItem	SimplePicture	\N	XyHxdHsi0V1GRzzC9NaU	\N	0	\N	\N	2017-06-29 04:51:30.281626	2017-06-29 04:51:30.281626	\N	\N	\N
46	123.jpg	\N	\N	\N	8	Product	Picture	\N	YDeDFwrtyvxxM5lE7O4m	\N	0	\N	\N	2017-06-29 04:52:55.053884	2017-06-29 04:53:35.218889	f	\N	\N
48	10.jpg	\N	\N	\N	8	Product	Picture	\N	DvCR2pg8sBgQpyfKM0bX	\N	0	\N	\N	2017-06-29 04:53:35.234982	2017-06-29 04:53:35.234982	f	\N	\N
49	10.jpg	\N	\N	\N	7	Product	Picture	\N	XEf1ppCUJbw0uy0Y69Bd	\N	0	\N	\N	2017-06-29 04:54:28.067396	2017-06-29 04:54:28.067396	f	\N	\N
50	123.jpg	\N	\N	\N	7	Product	Picture	\N	updiegWMG1aU9OFviBSQ	\N	0	\N	\N	2017-06-29 04:54:28.082471	2017-06-29 04:54:28.082471	f	\N	\N
51	123.jpg	\N	\N	\N	6	Product	Picture	\N	x87mpN7NLqYUqGOtaZhq	\N	0	\N	\N	2017-06-29 04:54:52.364125	2017-06-29 04:54:52.364125	f	\N	\N
52	10.jpg	\N	\N	\N	6	Product	Picture	\N	gRIyik5Rm5Ir03exh8kO	\N	0	\N	\N	2017-06-29 04:54:52.38193	2017-06-29 04:54:52.38193	f	\N	\N
53	123.jpg	\N	\N	\N	5	Product	Picture	\N	vpF592yymG4Sb0w1xkOk	\N	0	\N	\N	2017-06-29 04:55:12.168593	2017-06-29 04:55:12.168593	f	\N	\N
54	123.jpg	\N	\N	\N	5	Product	Picture	\N	vYex0EzjywWxnEsyx8Jh	\N	0	\N	\N	2017-06-29 04:55:12.178834	2017-06-29 04:55:12.178834	f	\N	\N
40	1466770343576d23a709b2c6.73832744.jpg	\N	\N	\N	1	Home	Teaser	\N	xSeJHifxWNWkJECB44lg	\N	0	\N	\N	2017-06-29 00:21:54.132217	2017-06-30 05:01:32.154988	\N	ТЕСТОВЫЙ ЗАГОЛОВОК	/contacts
\.


--
-- Name: assets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: localuser
--

SELECT pg_catalog.setval('assets_id_seq', 58, true);


--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: localuser
--

COPY categories (id, title, description, slug, created_at, updated_at, "position") FROM stdin;
1	Люльки	            <div class="ui stackable grid">\r\n               <div class="six wide column">\r\n                  <img src="/images/catalog_about.png" alt="" class="ui image">\r\n               </div>\r\n               <div class="ten wide column">\r\n                  <div class="catalog-content">\r\n                     <p>Компания Вира-Трейд предлагает свои услуги по продаже, монтажу и сервисному обслуживанию такого незаменимого во многих сферах оборудования как монтажные люльки, их еще называют подъемные корзины или строительные корзины. Наше предложение характеризуется целым рядом важных преимуществ, обеспечивающих максимальную эффективность приобретения этого оборудования.</p>\r\n                     <div class="ui two column stackable grid">\r\n                        <div class="column">\r\n                           <div class="header">Назначение:</div>\r\n                           <ul class="styled-list">\r\n                              <li><span>Проведение высотных монтажных работ;<span></span></span></li>\r\n                              <li><span>Ремонт фасадов и установка рекламы;<span></span></span></li>\r\n                              <li><span>Удаление сосулек с козырьков крыш;<span></span></span></li>\r\n                              <li><span>Замена ламп уличного освещения;<span></span></span></li>\r\n                              <li><span>Спиливание сухих веток и т.п.<span></span></span></li>\r\n                           </ul>\r\n                        </div>\r\n                        <div class="column">\r\n                           <div class="header">Приемущества:</div>\r\n                           <ul class="styled-list">\r\n                              <li><span>Проведение высотных монтажных работ;<span></span></span></li>\r\n                              <li><span>Ремонт фасадов и установка рекламы;<span></span></span></li>\r\n                              <li><span>Удаление сосулек с козырьков крыш;<span></span></span></li>\r\n                              <li><span>Замена ламп уличного освещения;<span></span></span></li>\r\n                              <li><span>Спиливание сухих веток и т.п.<span></span></span></li>\r\n                           </ul>\r\n                        </div>\r\n                     </div>\r\n                  </div>\r\n               </div>\r\n            </div>	lulki	2017-06-26 19:23:38.395062	2017-06-30 06:09:15.076031	1
2	Миникраны	            <div class="ui stackable grid">\r\n               <div class="six wide column">\r\n                  <img src="/images/catalog_about.png" alt="" class="ui image">\r\n               </div>\r\n               <div class="ten wide column">\r\n                  <div class="catalog-content">\r\n                     <p>Компания Вира-Трейд предлагает свои услуги по продаже, монтажу и сервисному обслуживанию такого незаменимого во многих сферах оборудования как монтажные люльки, их еще называют подъемные корзины или строительные корзины. Наше предложение характеризуется целым рядом важных преимуществ, обеспечивающих максимальную эффективность приобретения этого оборудования.</p>\r\n                     <div class="ui two column stackable grid">\r\n                        <div class="column">\r\n                           <div class="header">Назначение:</div>\r\n                           <ul class="styled-list">\r\n                              <li><span>Проведение высотных монтажных работ;<span></span></span></li>\r\n                              <li><span>Ремонт фасадов и установка рекламы;<span></span></span></li>\r\n                              <li><span>Удаление сосулек с козырьков крыш;<span></span></span></li>\r\n                              <li><span>Замена ламп уличного освещения;<span></span></span></li>\r\n                              <li><span>Спиливание сухих веток и т.п.<span></span></span></li>\r\n                           </ul>\r\n                        </div>\r\n                        <div class="column">\r\n                           <div class="header">Приемущества:</div>\r\n                           <ul class="styled-list">\r\n                              <li><span>Проведение высотных монтажных работ;<span></span></span></li>\r\n                              <li><span>Ремонт фасадов и установка рекламы;<span></span></span></li>\r\n                              <li><span>Удаление сосулек с козырьков крыш;<span></span></span></li>\r\n                              <li><span>Замена ламп уличного освещения;<span></span></span></li>\r\n                              <li><span>Спиливание сухих веток и т.п.<span></span></span></li>\r\n                           </ul>\r\n                        </div>\r\n                     </div>\r\n                  </div>\r\n               </div>\r\n            </div>	minikrani	2017-06-27 06:04:47.507275	2017-06-30 06:09:50.08229	2
\.


--
-- Name: categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: localuser
--

SELECT pg_catalog.setval('categories_id_seq', 2, true);


--
-- Data for Name: ckeditor_assets; Type: TABLE DATA; Schema: public; Owner: localuser
--

COPY ckeditor_assets (id, data_file_name, data_content_type, data_file_size, type, width, height, created_at, updated_at) FROM stdin;
6	hino.png	image/png	3154	\N	61	63	2017-06-29 00:58:35.638077	2017-06-29 00:58:35.638077
7	kam.png	image/png	3789	\N	46	55	2017-06-29 00:58:35.691246	2017-06-29 00:58:35.691246
8	int.png	image/png	11456	\N	150	48	2017-06-29 00:58:35.736319	2017-06-29 00:58:35.736319
9	ural.png	image/png	8241	\N	125	61	2017-06-29 00:59:07.548669	2017-06-29 00:59:07.548669
10	pal.png	image/png	7766	\N	150	34	2017-06-29 00:59:07.589671	2017-06-29 00:59:07.589671
11	oil.png	image/png	11403	\N	150	41	2017-06-29 00:59:07.629964	2017-06-29 00:59:07.629964
12	pr3.png	image/png	3853	\N	80	78	2017-06-29 01:01:45.616919	2017-06-29 01:01:45.616919
13	pr2.png	image/png	4562	\N	108	78	2017-06-29 01:01:45.623033	2017-06-29 01:01:45.623033
14	pr1.png	image/png	2520	\N	67	79	2017-06-29 01:01:45.706657	2017-06-29 01:01:45.706657
16	pr4.png	image/png	5351	\N	86	77	2017-06-29 01:02:05.796692	2017-06-29 01:02:05.796692
17	cog.png	image/png	441	\N	14	14	2017-06-29 01:04:08.713397	2017-06-29 01:04:08.713397
18	about.png	image/png	274769	\N	427	426	2017-06-30 04:59:58.179984	2017-06-30 04:59:58.179984
\.


--
-- Name: ckeditor_assets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: localuser
--

SELECT pg_catalog.setval('ckeditor_assets_id_seq', 18, true);


--
-- Data for Name: contacts; Type: TABLE DATA; Schema: public; Owner: localuser
--

COPY contacts (id, content, created_at, updated_at) FROM stdin;
1	<div class="ui header header--red fz24">КОНТАКТЫ</div>\r\n\r\n<p class="fz18">Единый телефон для справок: 8&nbsp;(800) 555-39-86 (звонок бесплатный)<br />\r\nЕдиный адрес электронной почты: info@vira-trade.ru</p>\r\n\r\n<div class="contacts-item">\r\n<div class="ui header fz24">Центральный офис в Санкт-Петербурге</div>\r\n<script charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A25c419ee0d92aae2ad5f763b269e08c77f294a90744f4cc2f122e01d3a7f6103&amp;width=100%25&amp;height=364&amp;lang=ru_RU&amp;scroll=true"></script>\r\n\r\n<div class="ui two column stackable grid contacts-meta fz18">\r\n<div class="column contacts-data">+7 (812) 946 89-86, spb@vira-trade.ru</div>\r\n\r\n<div class="column address">194244, Санкт-Петербург, Витебский пр. 31, корпус 2, литер А<br />\r\nПН-ПТ с 9:00 до 18:00</div>\r\n</div>\r\n</div>\r\n\r\n<div class="contacts-item">\r\n<div class="ui header fz24">Филиал в Москве</div>\r\n<script charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A56823e1f4ae3a5d08c505d05092cc295a1c66d78c43dd8f6e9ea1a132422ed90&amp;width=100%25&amp;height=396&amp;lang=ru_RU&amp;scroll=true"></script>\r\n\r\n<div class="ui two column stackable grid contacts-meta fz18">\r\n<div class="column contacts-data">+7 (499) 653-84-63, msk@vira-trade.ru</div>\r\n\r\n<div class="column address">115114 г. Москва, ул. Летниковская, д. 16<br />\r\nПН-ПТ с 9:00 до 18:00</div>\r\n</div>\r\n</div>\r\n\r\n<div class="contacts-item">\r\n<div class="ui header fz24">Региональный офис в Екатеринбурге</div>\r\n<script charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A56823e1f4ae3a5d08c505d05092cc295a1c66d78c43dd8f6e9ea1a132422ed90&amp;width=100%25&amp;height=396&amp;lang=ru_RU&amp;scroll=true"></script>\r\n\r\n<div class="ui two column stackable grid contacts-meta fz18">\r\n<div class="column contacts-data">+7 (343) 351-75-66, ekb@vira-trade.ru</div>\r\n\r\n<div class="column address">620085 Екатеринбург, ул. Братская, д. 27/3, подъезд 2<br />\r\nПН-ПТ с 9:00 до 18:00</div>\r\n</div>\r\n</div>\r\n\r\n<div class="about-company">\r\n<div class="ui header header--red fz24">О КОМПАНИИ</div>\r\n\r\n<p class="fz18">&laquo;Вира-Трейд&raquo; - компания, предоставляющая полный комплекс услуг по производству, поставке и ремонту спецавтотехники. Компания &laquo;Вира-Трейд&raquo; осуществляет поставку и обслуживание техники на всей территории РФ, работает с такими крупными заказчиками как: &laquo;Сургутнефтегаз&raquo;, &laquo;Роснефть&raquo;, &laquo;Лукойл&raquo;, &laquo;СтройГазКонсалтинг&raquo;, а также предприятиями энергетического комплекса Северо-Западного, Центрального, Приволжского и Уральского региона. Наши сотрудники имеют огромный опыт в области производства и реализации спецавтотехники таких производителей как: Oil&amp;Steel, КАМАЗ, ГАЗ, Урал, Нефаз, Hyundai,MAN, Hino, Fusso, МАЗ, &laquo;Ивановская марка&raquo;, Челябинец, Галичанин, Fassi, Hiab, Palfinger, PM, что позволяет предоставить заказчику информацию о том товаре, который наилучшим образом подойдёт для выполнения поставленных задач. Компания &laquo;Вира-Трейд&raquo; постоянно развивает творческий и интеллектуальный потенциал, наращивает техническую и производственную базы. Выверенный алгоритм проектирования и производства позволяет специалистам Компании решать практически любые задачи в четком взаимодействии с Заказчиком.</p>\r\n\r\n<div class="ui two column stackable grid meta">\r\n<div class="column">\r\n<div class="ui header fz18">Юридический адрес и реквизиты:</div>\r\n\r\n<p class="fz18">ОOО &quot;Aльтиc Груп&quot; 620024 Eкатeринбург, ул. Hoвинская, 13, oфис 205, ИHН 6679031313, KПП 667901001, OГРH 1136679004923, OКПО 25066477, р/с 40702810722560001908, в ФAКБ &laquo;АБСOЛЮТ БAНК&raquo; (ОАО), г. Eкатеринбург, к/с 30101810800000000921, БИK 046577921, дирeктoр Злoбин Eгoр Вячeслaвoвич.</p>\r\n</div>\r\n\r\n<div class="column">\r\n<div class="ui header fz18">Aдрес для кoррeспoндeнции:</div>\r\n\r\n<p class="fz18">620062, Екатеринбург, ул. Tимирязeва, 13, a/я 18</p>\r\n</div>\r\n</div>\r\n</div>\r\n	2017-06-29 02:40:02.71511	2017-06-29 02:44:26.125586
\.


--
-- Name: contacts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: localuser
--

SELECT pg_catalog.setval('contacts_id_seq', 1, true);


--
-- Data for Name: homes; Type: TABLE DATA; Schema: public; Owner: localuser
--

COPY homes (id, created_at, updated_at, about) FROM stdin;
1	2017-06-28 00:23:46.589807	2017-06-30 05:01:12.532134	<div class="ui container">\r\n<div class="about">\r\n<h2 class="ui header header--red">О КОМПАНИИ</h2>\r\n\r\n<div class="content">\r\n<div class="ui grid">\r\n<div class="six wide computer sixteen wide mobile column"><img alt="" src="/uploads/ckeditor/pictures/18/content_about.png" style="width: 427px; height: 426px;" /></div>\r\n\r\n<div class="ten wide computer sixteen wide mobile column">\r\n<p class="first">&laquo;Вира-Трейд&raquo; компания, предоставляющая полный комплекс услуг по производству, поставке и ремонту спецавтотехники.</p>\r\n\r\n<h3 class="ui header">Услуги</h3>\r\n\r\n<p>Компания &laquo;Вира-Трейд&raquo; осуществляет поставку и обслуживание техники на всей территории РФ, работает с такими крупными заказчиками как: &laquo;Сургутнефтегаз&raquo;, &laquo;Роснефть&raquo;, &laquo;Лукойл&raquo;, &laquo;СтройГазКонсалтинг&raquo;, а также предприятиями энергетического комплекса Северо-Западного, Центрального, Приволжского и Уральского региона.</p>\r\n\r\n<h3 class="ui header">Опыт</h3>\r\n\r\n<p>Наши сотрудники имеют огромный опыт в области производства и реализации спецавтотехники таких производителей как: Oil&amp;Steel, КАМАЗ, ГАЗ, Урал, Нефаз, Hyundai,MAN, Hino, Fusso, МАЗ, &laquo;Ивановская марка&raquo;, Челябинец, Галичанин, Fassi, Hiab, Palfinger, PM, что позволяет предоставить заказчику информацию о том товаре, который наилучшим образом подойдёт для выполнения поставленных задач.</p>\r\n\r\n<h3 class="ui header">Качество обслуживания</h3>\r\n\r\n<p>Компания &laquo;Вира-Трейд&raquo; постоянно развивает творческий и интеллектуальный потенциал, наращивает техническую и производственную базы. Выверенный алгоритм проектирования и производства позволяет специалистам Компании решать практически любые задачи в четком взаимодействии с Заказчиком.</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n
\.


--
-- Name: homes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: localuser
--

SELECT pg_catalog.setval('homes_id_seq', 1, true);


--
-- Data for Name: news_items; Type: TABLE DATA; Schema: public; Owner: localuser
--

COPY news_items (id, title, short_desc, content, image, created_at, updated_at) FROM stdin;
1	Hiab 1200RS для работы с отходами	<h2 style="font-style:italic;"><span style="font-size:16px;">Известный производитель кранов-манипуляторов компания HIAB выпустила новый кран Jonsered 1200RS для работы с песком, гравием и другими сыпучими материалами.</span></h2>\r\n	<p>Впервые кран был представлен публике на выставке IAA в Ганновере. Новый 11,3 тонный кран предназначен для сбора остатков песка и грунта после производства землеройных работ. Кран также может использоваться для перемещения и транспортировки песка и гравия. Система безопасности в 1200RS включает функцию Variable Stability Limit (VSL), которая следит за тем, насколько эффективно осуществляется контакт лап стабилизаторов с поверхностью грунта. Создается &laquo;виртуальная клетка безопасности&raquo; для оператора, не позволяющая стреле приближаться к этому пространству. В кране снижен расход топлива, продлен срок службы масла, улучшена эргономика. Вице-президент компании Джон Лопес считает, что новый кран &ndash; удобное и безопасное решение для работы в городской среде, где с каждым годом растет потребность в сборе и утилизации мусора и грунта после выполнения землеройных работ. В ближайшем времени новинка поступит в продажу. За более подробной информацией и для предварительного заказа нового крана обращайтесь в компанию &quot;Вира Трейд&quot;<br />\r\n&nbsp; &nbsp;&nbsp;</p>\r\n	\N	2017-06-29 03:32:30.492031	2017-06-29 03:39:12.489753
\.


--
-- Name: news_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: localuser
--

SELECT pg_catalog.setval('news_items_id_seq', 1, true);


--
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: localuser
--

COPY products (id, title, price, status, description1, description2, created_at, updated_at, category_id, short_desc, charact) FROM stdin;
6	test2	333.0000000000	0	<h3>Заголовок 1</h3>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit officiis voluptas debitis rem error aliquam laudantium neque minima velit, odit eaque corporis obcaecati optio nobis dolor, porro dolore nihil nulla. Nesciunt adipisci molestias aliquam quas illum optio necessitatibus saepe sequi ad in fugit voluptates quibusdam assumenda quaerat officiis porro neque aspernatur at magnam explicabo, ullam distinctio error nisi enim sint. Voluptatem quaerat nostrum eum qui quae incidunt in, possimus eveniet, porro optio, perferendis excepturi culpa fugiat cumque debitis asperiores exercitationem numquam vel placeat odit. A architecto, tenetur corporis debitis voluptate autem suscipit quibusdam, numquam veritatis molestiae fugiat, ipsum mollitia illo.</p>\r\n	<h3>Заголовок 2</h3>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit officiis voluptas debitis rem error aliquam laudantium neque minima velit, odit eaque corporis obcaecati optio nobis dolor, porro dolore nihil nulla. Nesciunt adipisci molestias aliquam quas illum optio necessitatibus saepe sequi ad in fugit voluptates quibusdam assumenda quaerat officiis porro neque aspernatur at magnam explicabo, ullam distinctio error nisi enim sint. Voluptatem quaerat nostrum eum qui quae incidunt in, possimus eveniet, porro optio, perferendis excepturi culpa fugiat cumque debitis asperiores exercitationem numquam vel placeat odit. A architecto, tenetur corporis debitis voluptate autem suscipit quibusdam, numquam veritatis molestiae fugiat, ipsum mollitia illo.</p>\r\n	2017-06-27 02:10:03.176928	2017-06-27 05:57:32.596921	1	<p>Макс. 1500 г/п., кг,<br />\r\nШирина вил от 500 до 900 мм,<br />\r\nМакс. высота груза от 1100 до 1700 мм.</p>\r\n	Наименование|Универсальная подвесная корзина\r\nПрименение|Краны-манипуляторы, автокраны, мостовые краны, козловые краны и т.д.\r\nСпособ установки|На крюк\r\nСоответствие конструкции нормативным документам|Приказ РОСТЕХНАДЗОРА № 533\r\nКоличество человек|2\r\nГрузоподъемность|от 250 до 500 кг (опционально)\r\nСобственный вес|не более 200 кг\r\nРазмеры корзины|(ДхШхВ)\t1400х800х2300 мм\r\nВысота борта|1100\r\nДополнительные опции|Входная калитка с засовом\r\nГотовность|В производстве
7	cxz	0.0000000000	0			2017-06-28 01:00:41.767152	2017-06-28 01:00:41.767152	1	<p>czxczx</p>\r\n	asasdasd
8	54t4xvcxvxv	0.0000000000	0	<p>sdf</p>\r\n	<p>sdfsdf</p>\r\n	2017-06-28 01:01:16.646355	2017-06-28 01:01:16.646355	2	<p>xvcxcvxc</p>\r\n	fgfg
5	test1	0.0000000000	1			2017-06-27 02:08:22.566584	2017-06-28 01:10:00.416545	1	<p>Макс. 1500 г/п., кг,<br />\r\nШирина вил от 500 до 900 мм,<br />\r\nМакс. высота груза от 1100 до 1700 мм.</p>\r\n	dfsdfsdf
\.


--
-- Name: products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: localuser
--

SELECT pg_catalog.setval('products_id_seq', 8, true);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: localuser
--

COPY schema_migrations (version) FROM stdin;
20170626115751
20170626121217
20170626142004
20170626143511
20170626154626
20170627010417
20170627010418
20170627010419
20170627010420
20170627010421
20170627010422
20170627183737
20170627193950
20170628192607
20170628210238
20170628212620
20170628215542
20170628225243
20170629234611
20170630000257
20170630001815
20170630005435
\.


--
-- Data for Name: service_spares; Type: TABLE DATA; Schema: public; Owner: localuser
--

COPY service_spares (id, title, content, created_at, updated_at) FROM stdin;
1	Запчасти для КМУ Palfinger	<div class="ui two column stackable grid">\r\n<div class="column">\r\n<div class="ui header fz24">Запчасти для КМУ Palfinger</div>\r\n\r\n<p class="fz14">Компания &laquo;Вира-Трейд&raquo; реализует запасные части для крано-манипуляторного оборудования фирмы &laquo;Палфингер&raquo; по выгодным ценам. Есть свой склад запчастей Palfinger, осуществляется доставка. Регулярные поступления позволяют поддерживать максимально полную номенклатуру, включая дефицитные позиции. По телефону 8 (800) 55-539-86 специалисты &laquo;Вира-Трейд&raquo; подберут комплектующие для любых крано-манипуляторных установок, примут заявку, расскажут о гарантиях и сервисном обслуживании.</p>\r\n</div>\r\n\r\n<div class="column spare-meta">\r\n<div class="spare-icon"><img alt="" class="ui image" src="/uploads/ckeditor/pictures/10/content_pal.png" /></div>\r\n\r\n<h3 class="ui header fz20">Вы можете купить:</h3>\r\n\r\n<ul class="styled-list">\r\n\t<li>Детали, узлы</li>\r\n\t<li>Эксплуатационно-расходные жидкости и материалы</li>\r\n\t<li>Элементы в сборе;</li>\r\n\t<li>Прочие виды запчастей Palfinger.</li>\r\n</ul>\r\n\r\n<div class="ui big red fluid button fz18 show-modal">ОТПРАВИТЬ ЗАЯВКУ</div>\r\n</div>\r\n</div>\r\n\r\n<div class="ui one column grid">\r\n<div class="row">\r\n<div class="column">\r\n<div class="ui header">Запчасти в наличии</div>\r\n</div>\r\n</div>\r\n\r\n<div class="row">\r\n<div class="column">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus dignissimos saepe, perferendis repellat, reprehenderit deserunt rem! Nam laborum dolor cupiditate a inventore, rerum optio quas numquam deserunt officiis natus dolores asperiores perspiciatis atque, mollitia veniam, laudantium explicabo voluptate reiciendis. In praesentium corporis magni unde sunt voluptas nobis. Saepe illum suscipit nulla dolorem ipsum corporis, iste qui mollitia delectus natus quam aut nihil reprehenderit. Et quibusdam id fugiat voluptatibus sunt eos sed, similique impedit possimus. Expedita quam minus et alias fugit deleniti dolorum quis amet aut libero maiores, mollitia consectetur ab rem sunt ullam cupiditate quas nemo ex maxime officiis distinctio.</div>\r\n</div>\r\n</div>\r\n	2017-06-29 02:11:57.180258	2017-06-29 02:17:29.838764
\.


--
-- Name: service_spares_id_seq; Type: SEQUENCE SET; Schema: public; Owner: localuser
--

SELECT pg_catalog.setval('service_spares_id_seq', 1, true);


--
-- Data for Name: services; Type: TABLE DATA; Schema: public; Owner: localuser
--

COPY services (id, content, created_at, updated_at) FROM stdin;
1	<div class="ui header header--red fz24 underline">ЗАПЧАСТИ И СЕРВИС</div>\r\n\r\n<h2 class="ui header">Запчасти</h2>\r\n\r\n<div class="spare-icons">\r\n<div class="ui two column centered grid">\r\n<div class="row">\r\n<div class="column"><a href="http://localhost:3000/service/1"><img class="ui image logo1" src="/uploads/ckeditor/pictures/10/content_pal.png" /></a></div>\r\n\r\n<div class="column"><img class="ui image logo2" src="/uploads/ckeditor/pictures/11/content_oil.png" /></div>\r\n</div>\r\n\r\n<div class="row">\r\n<div class="column"><img class="ui image logo3" src="/uploads/ckeditor/pictures/8/content_int.png" /></div>\r\n\r\n<div class="column"><img class="ui image logo4" src="/uploads/ckeditor/pictures/9/content_ural.png" /></div>\r\n</div>\r\n\r\n<div class="row">\r\n<div class="column"><img class="ui image logo5" src="/uploads/ckeditor/pictures/6/content_hino.png" /></div>\r\n\r\n<div class="column"><img class="ui image logo6" src="/uploads/ckeditor/pictures/7/content_kam.png" /></div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<p class="service">&nbsp;</p>\r\n\r\n<div class="ui header fz20">ООО &laquo;Вира-Трейд&raquo;</div>\r\n\r\n<p>Наша компания осуществляет техническую поддержку проданного оборудования, поставку запасных частей и расходных материалов. У нас вы всегда сможете приобрести запасные части и расходные материалы на всю поставляемую продукцию. Большой склад запасных частей и налаженный механизм отправки товара в регионы, позволяет работать с клиентами расположенными в самых отдалённых уголках России. Квалифицированные менеджеры помогут выбрать необходимые детали. Одним из преимуществ нашей компании является высокая скорость оформления и отправки товара, что особенно важно в случае непредвиденного ремонта.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2 class="ui header">Сервис</h2>\r\n\r\n<p>Наша компания предлагает широкий спектр услуг по ремонту и обслуживанию спецавтотехники отечественного и зарубежного производства. Многолетний опыт работы, квалифицированный персонал, уникальное ремонтное и диагностическое оборудование позволяют производить обслуживание любой сложности.</p>\r\n\r\n<div class="ui header fz20">Услуги:</div>\r\n\r\n<div class="ui two column stackable grid service-list">\r\n<div class="column">\r\n<div class="item"><img alt="" src="/uploads/ckeditor/pictures/17/content_cog.png" />Техническое обслуживание шасси КАМАЗ, МАЗ, Урал, Газ, Hino</div>\r\n\r\n<div class="item"><img alt="" src="/uploads/ckeditor/pictures/17/content_cog.png" />Техническое обслуживание КМУ, АГП</div>\r\n\r\n<div class="item"><img alt="" src="/uploads/ckeditor/pictures/17/content_cog.png" />Ремонт ходовой части</div>\r\n\r\n<div class="item"><img alt="" src="/uploads/ckeditor/pictures/17/content_cog.png" />Кузовной ремонт</div>\r\n\r\n<div class="item"><img alt="" src="/uploads/ckeditor/pictures/17/content_cog.png" />Замену масла в гидравлической системе</div>\r\n</div>\r\n\r\n<div class="column">\r\n<div class="item"><img alt="" src="/uploads/ckeditor/pictures/17/content_cog.png" />Ремонт гидроцилиндров, в т.ч. телескопических</div>\r\n\r\n<div class="item"><img alt="" src="/uploads/ckeditor/pictures/17/content_cog.png" />Диагностика и очистка гидросистемы</div>\r\n\r\n<div class="item"><img alt="" src="/uploads/ckeditor/pictures/17/content_cog.png" />Сервисное обслуживание</div>\r\n</div>\r\n</div>\r\n\r\n<div class="preim">\r\n<h2 class="ui centered header">ПРЕИМУЩЕСТВА</h2>\r\n\r\n<div class="items">\r\n<div class="ui four column doubling grid">\r\n<div class="column fz16">\r\n<div class="preim-img-wrap"><img alt="" class="ui centered image" src="/uploads/ckeditor/pictures/14/content_pr1.png" /></div>\r\n\r\n<p class="content">Обслуживание<br />\r\nлюбой сложности</p>\r\n</div>\r\n\r\n<div class="column fz16">\r\n<div class="preim-img-wrap"><img alt="" class="ui centered image" src="/uploads/ckeditor/pictures/13/content_pr2.png" /></div>\r\n\r\n<p class="content">Уникальное<br />\r\nоборудование</p>\r\n</div>\r\n\r\n<div class="column fz16">\r\n<div class="preim-img-wrap"><img alt="" class="ui centered image" src="/uploads/ckeditor/pictures/12/content_pr3.png" /></div>\r\n\r\n<p class="content">Выездной<br />\r\nсервис</p>\r\n</div>\r\n\r\n<div class="column fz16"><img alt="" class="ui centered image" src="/uploads/ckeditor/pictures/16/content_pr4.png" />\r\n<p class="content">Склад<br />\r\nзапасных частей</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n	2017-06-29 01:53:15.451507	2017-06-29 02:16:35.980188
\.


--
-- Name: services_id_seq; Type: SEQUENCE SET; Schema: public; Owner: localuser
--

SELECT pg_catalog.setval('services_id_seq', 1, true);


--
-- Data for Name: spec_items; Type: TABLE DATA; Schema: public; Owner: localuser
--

COPY spec_items (id, title, "desc", link, link_text, callback, "position", created_at, updated_at) FROM stdin;
1	КОНСУЛЬТАЦИЯ СПЕЦИАЛИСТА	Оформите заявку и наши специальсты свяжутся с Вами в течении 10 минут.	#	Оформить заявку	t	1	2017-06-29 04:39:03.098366	2017-06-29 04:39:03.098366
2	СПЕЦПРЕДЛОЖЕНИЕ	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt 	/products/6	Подробнее	f	2	2017-06-29 04:48:36.463058	2017-06-29 04:48:36.463058
3	HIAB 1200RS	Известный производитель кранов-манипуляторов компания HIAB выпустила новый кран.	/news_items/1	Подробнее	f	3	2017-06-29 04:50:46.437233	2017-06-29 04:50:46.437233
4	СЕРВИС И ЗАПЧАСТИ	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt 	/service	Подробнее	f	4	2017-06-29 04:51:30.279568	2017-06-29 04:51:30.279568
\.


--
-- Name: spec_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: localuser
--

SELECT pg_catalog.setval('spec_items_id_seq', 4, true);


--
-- Data for Name: specs; Type: TABLE DATA; Schema: public; Owner: localuser
--

COPY specs (id, title, "desc", "position", link, created_at, updated_at, price) FROM stdin;
1	МОНТАЖНЫЕ ЛЮЛЬКИ (КОРЗИНЫ)	Для любых кранов и КМУ	1	/products/6	2017-06-30 05:17:30.807914	2017-06-30 05:37:28.634266	70 000
3	МОНТАЖНЫЕ ЛЮЛЬКИ (КОРЗИНЫ)	Для любых кранов и КМУ	2	/products/6	2017-06-30 05:17:30.807914	2017-06-30 05:37:44.510448	90 000
2	МОНТАЖНЫЕ ЛЮЛЬКИ (КОРЗИНЫ)	Для любых кранов и КМУ	3	/products/6	2017-06-30 05:17:30.807914	2017-06-30 05:37:52.971192	80 000
\.


--
-- Name: specs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: localuser
--

SELECT pg_catalog.setval('specs_id_seq', 1, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: localuser
--

COPY users (id, email, encrypted_password, remember_created_at, sign_in_count, current_sign_in_at, last_sign_in_at, current_sign_in_ip, last_sign_in_ip, created_at, updated_at) FROM stdin;
1	admin@test.ru	$2a$11$t371s8z7YLGFxXfcRLal.ei5N76pCDfUl59o1yg/4f1ak52JnE8J.	\N	11	2017-06-29 05:23:35.591969	2017-06-29 04:57:35.032469	127.0.0.1	127.0.0.1	2017-06-26 12:02:14.357027	2017-06-29 05:23:35.592922
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: localuser
--

SELECT pg_catalog.setval('users_id_seq', 1, true);


--
-- Data for Name: youtubes; Type: TABLE DATA; Schema: public; Owner: localuser
--

COPY youtubes (id, link, "desc", "position", created_at, updated_at) FROM stdin;
1	<iframe width="1280" height="720" src="https://www.youtube.com/embed/kscn8wCDAvE" frameborder="0" allowfullscreen></iframe>	КАМАЗ 65117 с КМУ ИНМАН ИТ 180	1	2017-06-30 06:01:02.774751	2017-06-30 06:01:02.774751
2	<iframe width="1280" height="720" src="https://www.youtube.com/embed/jKBocFRsID0" frameborder="0" allowfullscreen></iframe>	КАМАЗ 65117 с КМУ ИНМАН ИТ 180	2	2017-06-30 06:02:04.882713	2017-06-30 06:02:04.882713
\.


--
-- Name: youtubes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: localuser
--

SELECT pg_catalog.setval('youtubes_id_seq', 2, true);


--
-- Name: ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: assets_pkey; Type: CONSTRAINT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY assets
    ADD CONSTRAINT assets_pkey PRIMARY KEY (id);


--
-- Name: categories_pkey; Type: CONSTRAINT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: ckeditor_assets_pkey; Type: CONSTRAINT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY ckeditor_assets
    ADD CONSTRAINT ckeditor_assets_pkey PRIMARY KEY (id);


--
-- Name: contacts_pkey; Type: CONSTRAINT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY contacts
    ADD CONSTRAINT contacts_pkey PRIMARY KEY (id);


--
-- Name: homes_pkey; Type: CONSTRAINT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY homes
    ADD CONSTRAINT homes_pkey PRIMARY KEY (id);


--
-- Name: news_items_pkey; Type: CONSTRAINT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY news_items
    ADD CONSTRAINT news_items_pkey PRIMARY KEY (id);


--
-- Name: products_pkey; Type: CONSTRAINT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: service_spares_pkey; Type: CONSTRAINT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY service_spares
    ADD CONSTRAINT service_spares_pkey PRIMARY KEY (id);


--
-- Name: services_pkey; Type: CONSTRAINT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY services
    ADD CONSTRAINT services_pkey PRIMARY KEY (id);


--
-- Name: spec_items_pkey; Type: CONSTRAINT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY spec_items
    ADD CONSTRAINT spec_items_pkey PRIMARY KEY (id);


--
-- Name: specs_pkey; Type: CONSTRAINT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY specs
    ADD CONSTRAINT specs_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: youtubes_pkey; Type: CONSTRAINT; Schema: public; Owner: localuser
--

ALTER TABLE ONLY youtubes
    ADD CONSTRAINT youtubes_pkey PRIMARY KEY (id);


--
-- Name: index_assets_on_assetable_type_and_type_and_assetable_id; Type: INDEX; Schema: public; Owner: localuser
--

CREATE INDEX index_assets_on_assetable_type_and_type_and_assetable_id ON assets USING btree (assetable_type, type, assetable_id);


--
-- Name: index_assets_on_guid; Type: INDEX; Schema: public; Owner: localuser
--

CREATE INDEX index_assets_on_guid ON assets USING btree (guid);


--
-- Name: index_assets_on_public_token; Type: INDEX; Schema: public; Owner: localuser
--

CREATE INDEX index_assets_on_public_token ON assets USING btree (public_token);


--
-- Name: index_assets_on_user_id; Type: INDEX; Schema: public; Owner: localuser
--

CREATE INDEX index_assets_on_user_id ON assets USING btree (user_id);


--
-- Name: index_categories_on_position; Type: INDEX; Schema: public; Owner: localuser
--

CREATE INDEX index_categories_on_position ON categories USING btree ("position");


--
-- Name: index_categories_on_slug; Type: INDEX; Schema: public; Owner: localuser
--

CREATE UNIQUE INDEX index_categories_on_slug ON categories USING btree (slug);


--
-- Name: index_ckeditor_assets_on_type; Type: INDEX; Schema: public; Owner: localuser
--

CREATE INDEX index_ckeditor_assets_on_type ON ckeditor_assets USING btree (type);


--
-- Name: index_products_on_category_id; Type: INDEX; Schema: public; Owner: localuser
--

CREATE INDEX index_products_on_category_id ON products USING btree (category_id);


--
-- Name: index_products_on_title; Type: INDEX; Schema: public; Owner: localuser
--

CREATE INDEX index_products_on_title ON products USING btree (title);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: localuser
--

CREATE UNIQUE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

